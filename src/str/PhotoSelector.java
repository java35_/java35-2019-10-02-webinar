package str;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhotoSelector {
//	public static String[] selectPictures(String[] pictures, String regex) {
//		String res = "";
//		for (int i = 0; i < pictures.length; i++) {
//			if (pictures[i].matches(".*" + regex + ".*"))
//				res += " " + pictures[i];
//		}
//		return res.trim().split(" ");
//	}
	
//	public static String[] selectPictures(String[] pictures, String regex) {
//		String res = "";
//		for (String str : pictures) {
//			if (str.matches(".*" + regex + ".*"))
//				res += " " + str;
//		}
//		return res.trim().split(" ");
//	}
	
//	public static String[] selectPictures(String[] pictures, String regex) {
//		Pattern pattern = Pattern.compile("[^ ]*" + regex + "[^ ]*");
//		String str = String.join(" ", pictures);
//		String res = "";
//		
//		Matcher matcher = pattern.matcher(str);
//		
//		while (matcher.find()) {
//			res += " " + matcher.group();
//		}
//		
//		return res.trim().split(" ");
//	}
	
	public static String[] selectPictures(String[] pictures, String regex) {
		Pattern pattern = Pattern.compile("[^ ]*" + regex + "[^ ]*");
		String str = Arrays.toString(pictures);
		String res = "";
		
		Matcher matcher = pattern.matcher(str);
		
		while (matcher.find()) {
			res += " " + matcher.group();
		}
		
		return res.trim().split(" ");
	}
}