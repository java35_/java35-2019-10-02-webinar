package str;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringTest {

	public static void main(String[] args) {
		
		// 1
		"Hello world!".equals("Hello world!");
		"Hello world!".equalsIgnoreCase("hello worlD!");
	
		System.out.println("Hello world!".equals("Hello world!"));
		System.out.println("Hello world!".equalsIgnoreCase("hello worlD!"));

		// 2
		"Hello world!".matches("Hello.*");
		System.out.println("Hello world!".matches("Hello.*"));
		
		// 3
		Pattern.matches("Hello.*", "Hello world!");
		System.out.println(Pattern.matches("Hello.*", "Hello world!"));
		
		// 4
		Pattern pattern = Pattern.compile("Hello.*");
		Matcher matcher = pattern.matcher("Hello world!");
		matcher.matches();
		System.out.println(matcher.matches());
	}
}