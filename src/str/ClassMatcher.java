package str;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClassMatcher {

	public static void main(String[] args) {
		Pattern pattern = Pattern.compile("[Hh]ello");
		Matcher matcher = pattern.matcher("hello Hello hello");
		
		while (matcher.find()) {
			System.out.println(matcher.start());
			System.out.println(matcher.end());
			System.out.println(matcher.group());
		}
		
		String str = "hello Hello hello";
		
		System.out.println(str.indexOf("hello"));
		System.out.println(str.indexOf("hello") + "hello".length());
		
		System.out.println(str.lastIndexOf("hello"));
		System.out.println(str.lastIndexOf("hello") + "hello".length());
	}
}