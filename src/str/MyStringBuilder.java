package str;

public class MyStringBuilder {
	public static void main(String[] args) {
//		String str = "Hello ";
//		str += "world!";
//
//		
//		StringBuilder sb = new StringBuilder();
//		System.out.println(sb);
//		System.out.println(sb.capacity());
//		System.out.println(sb.length());
//		System.out.println("*******************");		
//		StringBuilder sb1 = new StringBuilder("Hello world!");
//		System.out.println(sb1);
//		System.out.println(sb1.capacity());
//		System.out.println(sb1.length());
//		System.out.println("*******************");		
//		StringBuilder sb2 = new StringBuilder(10);
//		System.out.println(sb2);
//		System.out.println(sb2.capacity());
//		System.out.println(sb2.length());
		
		long start;
		long finish;
		
		String str = "";
		start = System.currentTimeMillis();
		for (int i = 0; i < 50_000; i++) {
			str += i;
		}
		finish = System.currentTimeMillis();
		System.out.println("String: " + (finish - start));
	}
}
