package str;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubString {

	public static void main(String[] args) {

		// 1
		"a A a".contains("A");
		System.out.println("a A a".contains("A"));

		// 2
		"a A a".matches(".*" + "A" + ".*");
		"a A a".matches(".*A.*");
		System.out.println("a A a".matches(".*" + "A" + ".*"));
		
		// 3
		Pattern.matches(".*" + "A" + ".*", "a A a");
		System.out.println(Pattern.matches(".*" + "A" + ".*", "a A a"));
	
		// 4
		Pattern pattern = Pattern.compile("A");
		Matcher matcher = pattern.matcher("a A a");
		System.out.println(matcher.find());
	}

}
