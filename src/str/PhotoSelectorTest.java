package str;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static str.PhotoSelector.selectPictures;

public class PhotoSelectorTest {
	private static final String[] pictures = {
			"Paris\\20140101_090000.jpg",
			"Paris\\20140201_121005.jpg",				
			"Paris\\20150301_211035.jpg",				
			"Paris\\20150401_110023.gif",
			"Paris\\20150401_181705.jpg",				
			"Paris\\20150501_231035.gif",				
			"London\\20140205_090000.jpg",
			"London\\20140205_121005.jpg",				
			"London\\20140601_211035.gif",				
			"London\\20151001_110023.jpg",
			"London\\20151001_121705.jpg",				
			"London\\20151001_231035.jpg",
			"Chicago\\20150301_120001.png",
			"Chicago\\20151111_232000.png"
	};

	@Test
	public void testAllEuropePictures() {
		String regex = "(London|Paris)";
		String[] expecteds = {
				"Paris\\20140101_090000.jpg",
				"Paris\\20140201_121005.jpg",				
				"Paris\\20150301_211035.jpg",				
				"Paris\\20150401_110023.gif",
				"Paris\\20150401_181705.jpg",				
				"Paris\\20150501_231035.gif",				
				"London\\20140205_090000.jpg",
				"London\\20140205_121005.jpg",				
				"London\\20140601_211035.gif",				
				"London\\20151001_110023.jpg",
				"London\\20151001_121705.jpg",				
				"London\\20151001_231035.jpg"	
		};
		String[] actuals = selectPictures(pictures, regex);
		assertArrayEquals(expecteds, actuals);
	}
	
	@Test
	public void testAllAutumnPictures() {
		String regex = "\\d{4}((09)|(1[01]))\\d{2}_";
		String[] expecteds = {
				"London\\20151001_110023.jpg",
				"London\\20151001_121705.jpg",				
				"London\\20151001_231035.jpg",
				"Chicago\\20151111_232000.png"
		};
		String[] actuals = selectPictures(pictures, regex);
		assertArrayEquals(expecteds, actuals);
	}
	
	@Test
	public void testAll2015SpringPictures() {
		String regex = "20150[345]\\d{2}_";
		String[] expecteds = {
				"Paris\\20150301_211035.jpg",				
				"Paris\\20150401_110023.gif",
				"Paris\\20150401_181705.jpg",				
				"Paris\\20150501_231035.gif",
				"Chicago\\20150301_120001.png"
		};
		String[] actuals = selectPictures(pictures, regex);
		assertArrayEquals(expecteds, actuals);
	}
	
	@Test
	public void testAllNightPictures() {
		String regex = "_((1[8-9])|(2[0-4]))";
		String[] expecteds = {
				"Paris\\20150301_211035.jpg",
				"Paris\\20150401_181705.jpg",				
				"Paris\\20150501_231035.gif",
				"London\\20140601_211035.gif",
				"London\\20151001_231035.jpg",
				"Chicago\\20151111_232000.png"
		};
		String[] actuals = selectPictures(pictures, regex);
		assertArrayEquals(expecteds, actuals);
	}
	
	@Test
	public void testAllNightPicturesFromChicago() {
		String regex = "Chicago\\\\\\d{8}_(1[8-9]|2[0-4])";
		String[] expecteds = {
				"Chicago\\20151111_232000.png"
		};
		String[] actuals = selectPictures(pictures, regex);
		assertArrayEquals(expecteds, actuals);
	}
	
	@Test
	public void testAllJpgAndPngPictures() {
		String regex = "\\.(jpg|png)";
		String[] expecteds = {
				"Paris\\20140101_090000.jpg",
				"Paris\\20140201_121005.jpg",				
				"Paris\\20150301_211035.jpg",				
				"Paris\\20150401_181705.jpg",				
				"London\\20140205_090000.jpg",
				"London\\20140205_121005.jpg",				
				"London\\20151001_110023.jpg",
				"London\\20151001_121705.jpg",				
				"London\\20151001_231035.jpg",
				"Chicago\\20150301_120001.png",
				"Chicago\\20151111_232000.png"
		};
		String[] actuals = selectPictures(pictures, regex);
		assertArrayEquals(expecteds, actuals);
	}
	
	

}
