package str;

public class StringBuilderTimeTest {

	public static void main(String[] args) {
		long start;
		long finish;
		
		StringBuilder str = new StringBuilder();
		start = System.currentTimeMillis();
		for (int i = 0; i < 50_000; i++) {
			str.append(i);
		}
		finish = System.currentTimeMillis();
		System.out.println("String: " + (finish - start));
	}

}
